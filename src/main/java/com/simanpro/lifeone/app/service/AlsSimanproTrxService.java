package com.simanpro.lifeone.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simanpro.lifeone.app.entity.AlsSimanproTrx;
import com.simanpro.lifeone.app.repository.AlsSimanproTrxRepository;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Service
public class AlsSimanproTrxService {

	@Autowired
	private AlsSimanproTrxRepository alsSimanproTrxRepository;
	
	@Transactional(readOnly = true)
	public List<AlsSimanproTrx> findAll() {
		
		return alsSimanproTrxRepository.findAll();
	}

	
	@Transactional(readOnly = true)
	public List<AlsSimanproTrx> getTrxByPais(String cvecta, String plann, String estado) {

		return (List<AlsSimanproTrx>) alsSimanproTrxRepository.findTrxByPais(cvecta, plann, estado);
	}
	

	@Transactional(readOnly = false)
	public Integer UpdateEstadoTrx(String cvecta, String plann, String estado, String newEstado) {
		
		return alsSimanproTrxRepository.UpdateEstadoTrx(cvecta, plann, estado, newEstado);
	}
	
	
	@Transactional(readOnly = false)
	public Integer UpdatePolizaByInvoiceNumber(String poliza, String invoiceNumber) {
		
		return alsSimanproTrxRepository.UpdatePolizaByInvoiceNumber(poliza, invoiceNumber);
	}
	
	
	@Transactional(readOnly = false)
	public Integer UpdateEstadoByInvoiceNumber(String estado, String invoiceNumber) {
		
		return alsSimanproTrxRepository.UpdateEstadoByInvoiceNumber(estado, invoiceNumber);
	}
	
}
