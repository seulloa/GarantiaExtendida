package com.simanpro.lifeone.app.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simanpro.lifeone.app.entity.AlsSimanproError;
import com.simanpro.lifeone.app.repository.AlsSimanproErrorRepository;

/**
 * @author sehulloa on 09-03-2022
 *
 */
@Service
public class AlsSimanproErrorService {
	
	@Autowired
	private AlsSimanproErrorRepository alsSimanproErrorRepository;
	
	@Transactional(readOnly = false)
	public AlsSimanproError create(AlsSimanproError alsSimanproError) {
		
		Long id = alsSimanproErrorRepository.getNextErrorId();
		alsSimanproError.setIdError(id);
		return alsSimanproErrorRepository.save(alsSimanproError);
	}
	
	

}
