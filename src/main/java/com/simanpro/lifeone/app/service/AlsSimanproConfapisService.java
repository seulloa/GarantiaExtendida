package com.simanpro.lifeone.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.simanpro.lifeone.app.entity.AlsSimanproConfapis;
import com.simanpro.lifeone.app.repository.AlsSimanproConfapisRepository;

/**
 * @author sehulloa on 08-03-2022
 *
 */
@Service
public class AlsSimanproConfapisService {

	@Autowired
	private AlsSimanproConfapisRepository alsSimanproConfapisRepository;
	
	
	@Transactional(readOnly = true)
	public Optional<AlsSimanproConfapis> findById(Long id) {
		return alsSimanproConfapisRepository.findById(id);
	}

}
