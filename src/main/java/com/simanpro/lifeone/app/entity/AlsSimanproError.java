package com.simanpro.lifeone.app.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Entity
@Table(
		name = "ALS_SIMANPRO_ERROR", 
		schema = "T2ATEST")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlsSimanproError implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ID_ERROR")
	private Long idError;
	
	@Column(name = "OBJECT_NAME")
	private String objectName;
	
	@Column(name = "DESCRIPTION")
	private String description;

	@Column(name = "DATE_TIME")
	private Date dateTime;
	
	@Column(name = "TRANSACTION_ID")
	private Long transactionId;
}
