package com.simanpro.lifeone.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Entity
@Table(
		name = "ALS_SIMANPRO_CONFAPIS", 
		schema = "T2ATEST")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlsSimanproConfapis implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "IDAPI")
	private Long idapi;

	@Column(name = "PAIS")
	private Integer pais;

	@Column(name = "URL")
	private String url;

	@Column(name = "TOKEN")
	private String token;

	@Column(name = "PROVEEDOR")
	private String proveedor;

	@Column(name = "ACTIVO")
	private Boolean activo;
}
