package com.simanpro.lifeone.app.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Entity
@Table(name = "ALS_SIMANPRO_TRX")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder(
		{ 
			"name", 
			"lastName", 
			"email", 
			"phone", 
			"identification", 
			"address", 
			"invoiceNumber", 
			"productId", 
			"clientId", 
			"dateInit", 
			"dateEnd", 
			"contractValue", 
			"categoryProduct", 
			"codStore", 
			"nameStore", 
			"boxStore", 
			"transaction",
			"plueProduct", 
			"plueGuarantee", 
			"imei", 
			"codSales", 
			"costElec", 
			"dateFacture", 
			"transactionDev" 
		})    
public class AlsSimanproTrx implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "INVOICENUMBER ")
	@JsonProperty("invoiceNumber")
	private String invoiceNumber;
	
	@Column(name = "ID_TRN")
	@JsonIgnore
	private Long idTrn;

	@Column(name = "CVECTA")
	@JsonProperty("clientId")
	private Long cvecta;

	@Column(name = "PLANN")
	@JsonProperty("productId")
	private Long plann;

	@Column(name = "APPTITULAR")
	@JsonProperty("lastName")
	private String apptitular;

	@Column(name = "APMTITULAR")
	@JsonIgnore
	private String apmtitular;

	@Column(name = "NOMTIT")
	@JsonProperty("name")
	private String nomtit;

	@Column(name = "CEDTIT")
	@JsonProperty("identification")
	private String cedtit;

	@Column(name = "CATPRO")
	@JsonProperty("categoryProduct")
	private String catpro;

	@Column(name = "PRODUCTO")
	@JsonIgnore
	private String producto;

	@Column(name = "FECINIV")
	@JsonIgnore
	private String feciniv;
	
	@Transient
	@JsonProperty("dateInit")
	private Date fechini;

	@Column(name = "FECFINV")
	@JsonIgnore
	private String fecfinv;
	
	@Transient
	@JsonProperty("dateEnd")
	private Date fechfin;
	
	@Column(name = "DIRTIT")
	@JsonProperty("address")
	private String dirtit;

	@Column(name = "TELFIT")
	@JsonProperty("phone")
	private String telfit;

	@Column(name = "CORREO")
	@JsonProperty("email")
	private String correo;

	@Column(name = "CODALM")
	@JsonProperty("codStore")
	private String codalm;

	@Column(name = "NOMALM")
	@JsonProperty("nameStore")
	private String nomalm;

	@Column(name = "CAJA")
	@JsonProperty("boxStore")
	private String caja;

	@Column(name = "TRANS")
	@JsonProperty("transaction")
	private String trans;

	@Column(name = "TRANSDEV")
	@JsonProperty("transactionDev")
	private String transdev;

	@Column(name = "PLUELEC")
	@JsonProperty("plueProduct")
	private String pluelec;

	@Column(name = "PLUGARA")
	@JsonProperty("plueGuarantee")
	private String plugara;

	@Column(name = "VALELEC")
	@JsonProperty("costElec")
	private BigDecimal valelec;

	@Column(name = "VALGARA")
	@JsonProperty("contractValue")
	private BigDecimal valgara;

	@Column(name = "IMEI")
	@JsonProperty("imei")
	private String imei;

	@Column(name = "CODVEN")
	@JsonProperty("codSales")
	private Long codven;

	@Column(name = "ESTADO")
	@JsonIgnore
	private Long estado;
	
	@Column(name = "DATEFACTURE")
	@JsonIgnore
	private String dateFacture;
	
	@Transient
	@JsonProperty("dateFacture")
	private Date fechfact;

	@Column(name = "POLIZA")
	@JsonIgnore
	private String poliza;
}
