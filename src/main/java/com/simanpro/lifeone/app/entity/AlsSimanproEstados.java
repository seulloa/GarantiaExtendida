package com.simanpro.lifeone.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Entity
@Table(name = "ALS_SIMANPRO_ESTADOS")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlsSimanproEstados implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ESTADO")
	private Long estado;

	@Column(name = "DESCRIPCION")
	private String descripcion;

}
