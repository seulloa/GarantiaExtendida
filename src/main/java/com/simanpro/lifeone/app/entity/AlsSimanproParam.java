package com.simanpro.lifeone.app.entity;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Entity
@Table(name = "ALS_SIMANPRO_PARAM")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AlsSimanproParam implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "PAIS")
	private byte pais;

	@Id
	@Column(name = "SUCURSAL")
	private String sucursal;

	@Column(name = "NOMBRE")
	private String nombre;

	@Column(name = "CVECTA")
	private BigInteger cvecta;

	@Column(name = "PLANN")
	private BigInteger plann;

}
