package com.simanpro.lifeone.app.client.response;

import lombok.Data;

@Data
public class AffiliateSure {
	
	private String invoiceNumber;
	private String productId;
	private String clientId;
	private String dateInit;
	private String dateEnd;
	private String contractValue;
	private String categoryProduct;
	private String codStore;
	private String nameStore;
	private String boxStore;
	private String transaction;
	private String transactionDev;
	private String plueProduct;
	private String plueGuarantee;
	private String imei;
	private String codSales;
	private String costElec;
	private String dateFacture;
	private String id;
	private String name;
	private String lastName;
	private String email;
	private String phone;
	private String identification;
	private String address;
	
}
