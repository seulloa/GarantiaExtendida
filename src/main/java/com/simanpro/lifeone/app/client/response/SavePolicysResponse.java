package com.simanpro.lifeone.app.client.response;

import lombok.Data;

@Data
public class SavePolicysResponse {
	
	private AffiliateSure affiliateSureDTO;
	private String policy;

}
