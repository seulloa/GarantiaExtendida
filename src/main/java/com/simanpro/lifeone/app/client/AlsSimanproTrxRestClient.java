package com.simanpro.lifeone.app.client;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.simanpro.lifeone.app.entity.AlsSimanproConfapis;
import com.simanpro.lifeone.app.service.AlsSimanproConfapisService;
import com.simanpro.lifeone.app.utils.FinalConstantParameter;

/**
 * @author sehulloa on 08-03-2022
 *
 */
@Controller
public class AlsSimanproTrxRestClient {

	@Autowired
	private AlsSimanproConfapisService alsSimanproConfapisService;

	RestTemplate restTemplate = new RestTemplate();

	/**
	 * Consume web service para verificar estado del servicio
	 * 
	 * @return
	 * @throws Exception
	 */
	public ResponseEntity<String> callGetStatus() {

		ResponseEntity<String> responseEntity = null;
		HttpHeaders headers = null;
		
		try {
			// Obtener la URL para revisar el estado del servicio
			String baseUri = alsSimanproConfapisService.findById(FinalConstantParameter.URL_BASE_LIFEONE).get()
					.getUrl();

			// Preparar REQUEST para WS status

			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<Object> requestEntity = new HttpEntity<>(headers);

			// Llamar WS status
			responseEntity = restTemplate.exchange(
					baseUri + FinalConstantParameter.URL_STATUS_LIFEONE, HttpMethod.GET, requestEntity, String.class);

			return responseEntity;
		} catch (ResourceAccessException es) {
			throw new ResourceAccessException(es.getMessage());
		} catch (Exception e) {
			String detailMessage = StringUtils.substringBefore(e.getMessage(), "; nested exception is");
			String status = detailMessage.substring(0,3);
			String jsonResponse = StringUtils.substringBetween(detailMessage, "{", "}");
			switch (status) {
			case "400":
				responseEntity = new ResponseEntity<>(jsonResponse, HttpStatus.BAD_REQUEST);
				break;
			case "401":
				responseEntity = new ResponseEntity<>(jsonResponse, HttpStatus.UNAUTHORIZED);
				break;
			case "405":
				responseEntity = new ResponseEntity<>(jsonResponse, HttpStatus.METHOD_NOT_ALLOWED);
				break;
			default:
				break;
			}
			return responseEntity;
		}
	}

	/**
	 * Consume web service para consolidar transacciones de ventas por país
	 * 
	 * @param jsonBody
	 * @param vPais
	 * @return
	 */
	public ResponseEntity<String> callSavePolicys(String jsonBody, String vPais) {

		ResponseEntity<String> responseEntity = null;
		Optional<AlsSimanproConfapis> alsSimanproConfapis = null;
		HttpHeaders headers = null;

		try {
			// Obtener la URL para revisar el estado del servicio
			Long confPais = obtenerConfApiPais(vPais);
			alsSimanproConfapis = alsSimanproConfapisService.findById(confPais);
			String baseUri = alsSimanproConfapis.get().getUrl();
			String token = alsSimanproConfapis.get().getToken();

			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", "Bearer " + token);

			HttpEntity<Object> requestEntity = new HttpEntity<>(jsonBody, headers);

			responseEntity = restTemplate.exchange(baseUri + FinalConstantParameter.URL_SAVEPOLICYS_LIFEONE,
					HttpMethod.POST, requestEntity, String.class);

			return responseEntity;
			
		} catch (Exception e) {
			String detailMessage = StringUtils.substringBefore(e.getMessage(), "; nested exception is");
			String status = detailMessage.substring(0,3);
			String jsonResponse = StringUtils.substringBetween(detailMessage, "{", "}");
			switch (status) {
			case "400":
				responseEntity = new ResponseEntity<>(jsonResponse, HttpStatus.BAD_REQUEST);
				break;
			case "401":
				responseEntity = new ResponseEntity<>(jsonResponse, HttpStatus.UNAUTHORIZED);
				break;
			case "405":
				responseEntity = new ResponseEntity<>(jsonResponse, HttpStatus.METHOD_NOT_ALLOWED);
				break;
			default:
				break;
			}
			
			return responseEntity;
		}
	}

	/**
	 * Obtiene la configuración por país para recuperar la URL y token correctos
	 * 
	 * @param codigoPais
	 * @return
	 * @throws Exception
	 */
	public Long obtenerConfApiPais(String codigoPais) throws Exception {

		Long confPais = null;

		switch (codigoPais) {
		case "SV":
			confPais = FinalConstantParameter.CONF_API_LIFEONE_SV;
			break;
		case "GT":
			confPais = FinalConstantParameter.CONF_API_LIFEONE_GT;
			break;
		case "NI":
			confPais = FinalConstantParameter.CONF_API_LIFEONE_NI;
			break;
		case "CR":
			confPais = FinalConstantParameter.CONF_API_LIFEONE_CR;
			break;
		}

		return confPais;
	}

}
