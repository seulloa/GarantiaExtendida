package com.simanpro.lifeone.app.controller;

import java.sql.SQLRecoverableException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.ResourceAccessException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.simanpro.lifeone.app.client.AlsSimanproTrxRestClient;
import com.simanpro.lifeone.app.client.response.SavePolicysResponse;
import com.simanpro.lifeone.app.entity.AlsSimanproError;
import com.simanpro.lifeone.app.entity.AlsSimanproTrx;
import com.simanpro.lifeone.app.service.AlsSimanproErrorService;
import com.simanpro.lifeone.app.service.AlsSimanproTrxService;
import com.simanpro.lifeone.app.utils.EstadoType;
import com.simanpro.lifeone.app.utils.FinalConstantParameter;

/**
 * @author sehulloa on 07-03-2022
 *
 */
@Controller
public class AlsSimanproTrxController {

	@Autowired
	private AlsSimanproTrxService alsSimanproTrxService;

	@Autowired
	AlsSimanproErrorService alsSimanproErrorService;

	@Autowired
	AlsSimanproTrxRestClient alsSimanproTrxRestClient;

	private Logger LOG = LoggerFactory.getLogger(AlsSimanproTrxController.class);

	/**
	 * Método principal que contiene toda la lógica del proceso
	 * 
	 * @param vPais
	 */
	public void ejecutarProceso(String vPais) {

		AlsSimanproError alsSimanproError = new AlsSimanproError();
		// Formato de fecha para el Json a enviar
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.mmm'Z'");

		try {

			// LLamar WSRest para obtener status de servicios
			LOG.info("CALLING WS LIFEONE: status");
			ResponseEntity<String> responseStatus = alsSimanproTrxRestClient.callGetStatus();

			if (responseStatus.getStatusCode() == HttpStatus.OK) {
				LOG.info("RESPONSE STATUS: " + responseStatus.getStatusCode());

				// Calcular las constantes por país
				Map<String, String> constPais = new HashMap<>();
				constPais = obtenerParametrosPais(vPais);

				LOG.info("Obteniendo la lista de ventas a consolidar");
				// Ejecutar consulta para recuperar todas las transacciones por país
				List<AlsSimanproTrx> alsSimanproTrxList = alsSimanproTrxService.getTrxByPais(constPais.get("cvecta"),
						constPais.get("plann"), String.valueOf(EstadoType.PENDIENTE_CONSOLIDACION.getCodigoEstado()));

				if (alsSimanproTrxList.size() > 0) {

					// Modificar el estado de los registros a PROCESO CONSOLIDACION
					LOG.info("MODIFY STATUS TRX: PROCESO CONSOLIDACION");
					actualizarEstadoTrx(constPais, String.valueOf(EstadoType.PENDIENTE_CONSOLIDACION.getCodigoEstado()),
							String.valueOf(EstadoType.PROCESO_CONSOLIDACION.getCodigoEstado()));

					// Gestionar fechas para envío correcto en Json
					for (AlsSimanproTrx trx : alsSimanproTrxList) {
						trx.setFechini(new SimpleDateFormat("dd/MM/yyyy").parse(trx.getFeciniv()));
						trx.setFechfin(new SimpleDateFormat("dd/MM/yyyy").parse(trx.getFecfinv()));
						trx.setFechfact(new SimpleDateFormat("dd/MM/yyyy").parse(trx.getDateFacture()));

					}

					// Convertir Lista de Trx a formato JSON
					ObjectMapper mapper = new ObjectMapper();
					mapper.setDateFormat(formater);

					// Crear string que contiene json a enviar con el WS
					String jsonSavePolicys = mapper.writeValueAsString(alsSimanproTrxList);
					LOG.info("JSON REQUEST SAVE_POLICYS: " + jsonSavePolicys);

					// LLamar WSRest para reportar transacciones
					// *****************************************
					LOG.info("CALLING WS LIFEONE: savePolicys");
					ResponseEntity<String> responseSavePolicys = alsSimanproTrxRestClient
							.callSavePolicys(jsonSavePolicys, vPais);

					// Procesar de acuerdo al código de estado en el response
					// ******************************************************
					switch (responseSavePolicys.getStatusCode()) {
					case OK:

						// Si la ejecución es correcta

						LOG.info("RESPONSE STATUS SAVE_POLICYS: " + responseSavePolicys.getStatusCode());
						LOG.info("RESPONSE JSON SAVE_POLICYS: " + responseSavePolicys.getBody());

						// Guardar la poliza para cada factura
						guardarPolizaTrx(responseSavePolicys, alsSimanproTrxList);

						// Modificar el estado de los registros a CONSOLIDADO
						LOG.info("MODIFY STATUS TRX: CONSOLIDADO");
						actualizarEstadoTrx(constPais,
								String.valueOf(EstadoType.PROCESO_CONSOLIDACION.getCodigoEstado()),
								String.valueOf(EstadoType.CONSOLIDADO.getCodigoEstado()));

						break;
					case METHOD_NOT_ALLOWED:
						LOG.info("RESPONSE STATUS SAVE_POLICYS: " + responseSavePolicys.getStatusCode()
								+ " Invalid affiliate supplied");
						break;
					default:
						LOG.info("RESPONSE STATUS SAVE_POLICYS: " + responseSavePolicys.getStatusCode());
						break;
					}

					if (responseSavePolicys.getStatusCode() != HttpStatus.OK) {

						switch (responseSavePolicys.getStatusCode()) {
						case BAD_REQUEST:

							LOG.info("RESPONSE JSON ERROR SAVE_POLICYS: " + responseSavePolicys.getBody());
							// Procesar lista de errores
							procesarListaErroresBadRequest(responseSavePolicys, alsSimanproTrxList);

							break;
						default:

							// Guardar el error
							alsSimanproError = new AlsSimanproError();

							alsSimanproError.setObjectName("ERROR");
							alsSimanproError.setDescription("REST Business Service returned HTTP response with status "
									+ responseSavePolicys.getStatusCode());
							alsSimanproError.setDateTime(new Date());
							alsSimanproError.setTransactionId(-1L);

							alsSimanproErrorService.create(alsSimanproError);

							// Cambiar el estado a PENDIENTE CONSOLIDACION a todos los registros
							for (AlsSimanproTrx trx : alsSimanproTrxList) {
								actualizarEstadoTrx(
										String.valueOf(EstadoType.PENDIENTE_CONSOLIDACION.getCodigoEstado()),
										trx.getInvoiceNumber());
							}
							break;
						}
						LOG.info("MODIFY STATUS TRX: ERROR");
					}

				} else {
					LOG.warn("No hay registros de transacciones en estado PENDIENTE CONSOLIDACION para " + vPais);
				}

			} else {
				LOG.info("RESPONSE STATUS: " + responseStatus.getStatusCode() + " Internal Server Error");
			}

		} catch (ResourceAccessException ex) {
			LOG.error("Conexión rechazada. " + ex.getMessage());
			// Guardar el error
			alsSimanproError = new AlsSimanproError();
			if (ex.getMessage() == null) {
				alsSimanproError.setDescription("Conexión rechazada.");
			} else {
				alsSimanproError.setDescription(ex.getMessage());
			}
			alsSimanproError.setObjectName("ERROR");
			alsSimanproError.setDateTime(new Date());
			alsSimanproError.setTransactionId(-1L);

			alsSimanproErrorService.create(alsSimanproError);
		} catch (SQLRecoverableException es) {
			LOG.error("Error relacionado con la Base de datos. " + es.getMessage());
			// Guardar el error
			alsSimanproError = new AlsSimanproError();
			if (es.getMessage() == null) {
				alsSimanproError.setDescription("Error de acceso a Base de datos.");
			} else {
				alsSimanproError.setDescription(es.getMessage());
			}
			alsSimanproError.setObjectName("ERROR");
			alsSimanproError.setDateTime(new Date());
			alsSimanproError.setTransactionId(-1L);

			alsSimanproErrorService.create(alsSimanproError);
		} catch (Exception e) {
			LOG.error("Error desconocido. " + e.getMessage());
			// Guardar el error
			alsSimanproError = new AlsSimanproError();

			String detailMessage = StringUtils.substringBefore(e.getMessage(), "; nested exception is");
			if (e.getMessage() == null) {
				alsSimanproError.setDescription("Unrecognized error");
			} else {
				alsSimanproError.setDescription(detailMessage);
			}
		}
	}

	/**
	 * Se utiliza para obtener el ID del cliente y del producto de acuerdo al país
	 * 
	 * @param codigoPais: SV, GT, NI, CR
	 * @return mapa con ambos valores cvecta y plann
	 */
	public Map<String, String> obtenerParametrosPais(String codigoPais) {

		// Se crea mapa donde se insertarán los valores
		Map<String, String> constPais = new HashMap<>();
		String cvecta = "";
		String plann = "";

		switch (codigoPais) {
		case "SV":
			cvecta = FinalConstantParameter.CVECTA_SV;
			plann = FinalConstantParameter.PLANN_SV;
			break;
		case "GT":
			cvecta = FinalConstantParameter.CVECTA_GT;
			plann = FinalConstantParameter.PLANN_GT;
			break;
		case "NI":
			cvecta = FinalConstantParameter.CVECTA_NI;
			plann = FinalConstantParameter.PLANN_NI;
			break;
		case "CR":
			cvecta = FinalConstantParameter.CVECTA_CR;
			plann = FinalConstantParameter.PLANN_CR;
			break;
		}

		// Insertar valores al mapa
		constPais.put("cvecta", cvecta);
		constPais.put("plann", plann);

		return constPais;
	}

	/**
	 * Método sobrecargado para cambiar estado a una transacción de acuerdo al país
	 * y que tengan un estado definido
	 * 
	 * @param constPais    Mapa con ambos valores cvecta y plann
	 * @param estadoBuscar
	 * @param nuevoEstado
	 */
	public void actualizarEstadoTrx(Map<String, String> constPais, String estadoBuscar, String nuevoEstado) {
		alsSimanproTrxService.UpdateEstadoTrx(constPais.get("cvecta"), constPais.get("plann"), estadoBuscar,
				nuevoEstado);
	}

	/**
	 * Método sobrecargado para cambiar estado a una transacción por medio de la
	 * factura
	 * 
	 * @param estado
	 * @param invoice
	 */
	public void actualizarEstadoTrx(String estado, String invoice) {

		alsSimanproTrxService.UpdateEstadoByInvoiceNumber(estado, invoice);
	}

	/**
	 * Guardar la lista de polizas recibidas como respuesta junto a estado 200 OK
	 * 
	 * @param responseSavePolicys
	 * @param alsSimanproTrxList
	 * @throws Exception
	 */
	public void guardarPolizaTrx(ResponseEntity<String> responseSavePolicys, List<AlsSimanproTrx> alsSimanproTrxList)
			throws Exception {

		List<SavePolicysResponse> savePolicysResponseList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();
		String vInvoice = "";
		String vPolicy = "";
		Long vTransId = 0L;

		String spResponse = responseSavePolicys.getBody();
		savePolicysResponseList = Arrays.asList(mapper.readValue(spResponse, SavePolicysResponse[].class));

		LOG.info("Guardando polizas recibidas");
		for (SavePolicysResponse policy : savePolicysResponseList) {
			vInvoice = policy.getAffiliateSureDTO().getInvoiceNumber();
			vPolicy = policy.getPolicy();

			// Buscar InvoiceNumber en la lista de transacciones enviada para guardar póliza
			for (AlsSimanproTrx trx : alsSimanproTrxList) {
				if (trx.getInvoiceNumber().equals(vInvoice)) {

					if (vPolicy.startsWith("PO-")) {

						// Si la poliza esta definida se guarda en la tabla de transacciones local
						alsSimanproTrxService.UpdatePolizaByInvoiceNumber(vPolicy, vInvoice);

					} else {
						// Si la poliza no esta definida se guarda el error correspondiente a la venta
						vTransId = trx.getIdTrn();

						AlsSimanproError alsSimanproError = new AlsSimanproError();
						alsSimanproError.setObjectName("ERROR");
						alsSimanproError.setDescription("HTTP response " + responseSavePolicys.getStatusCode()
								+ " : TRX_ID " + vTransId + " : policy " + vPolicy);
						alsSimanproError.setDateTime(new Date());
						alsSimanproError.setTransactionId(vTransId);

						// Se guarda el error
						alsSimanproErrorService.create(alsSimanproError);

						// Se coloca estado ERROR a la transacción
						actualizarEstadoTrx(String.valueOf(EstadoType.ERROR.getCodigoEstado()), vInvoice);
					}
				}
			}
		}
	}

	/**
	 * Método para procesar la lista de errores por response 400 Bad Request
	 * 
	 * @param responseSavePolicys
	 * @param alsSimanproTrxList
	 */
	public void procesarListaErroresBadRequest(ResponseEntity<String> responseSavePolicys,
			List<AlsSimanproTrx> alsSimanproTrxList) {

		AlsSimanproError alsSimanproError = new AlsSimanproError();

		LOG.info("Procesando lista de errores");
		// Guardar en una lista la respuesta del jsonResponse
		String spResponse = responseSavePolicys.getBody();

		// Cambiar el estado a PENDIENTE CONSOLIDACION a todos los registros
		for (AlsSimanproTrx trx : alsSimanproTrxList) {
			actualizarEstadoTrx(String.valueOf(EstadoType.PENDIENTE_CONSOLIDACION.getCodigoEstado()),
					trx.getInvoiceNumber());
		}

		if (spResponse != null) {
			String[] erroresList = spResponse.split(",");

			LOG.info("SAVING LOG ERROR");
			for (int i = 0; i < erroresList.length; i++) {

				String[] parts = erroresList[i].split(":");
				String part1 = parts[0].replaceAll("\"", "");
				String[] keysError = part1.split(Pattern.quote("."));

				String columnTab = keysError[2];
				int numReg = Integer.parseInt(StringUtils.substringBetween(keysError[1], "[", "]"));
				Long transId = alsSimanproTrxList.get(numReg).getIdTrn();
				String vInvoice = alsSimanproTrxList.get(numReg).getInvoiceNumber();

				String errorDesc = StringUtils.substringBetween(parts[1], "\"", "\"");

				// Guardar el error
				alsSimanproError = new AlsSimanproError();
				alsSimanproError.setObjectName("ERROR");
				alsSimanproError.setDescription("HTTP response " + responseSavePolicys.getStatusCode() + ": TRX_ID "
						+ transId + " : " + columnTab + " " + errorDesc);
				alsSimanproError.setDateTime(new Date());
				alsSimanproError.setTransactionId(transId);
				alsSimanproErrorService.create(alsSimanproError);

				// Se coloca estado ERROR a la transacción
				actualizarEstadoTrx(String.valueOf(EstadoType.ERROR.getCodigoEstado()), vInvoice);
			}

		} else {

			// Guardar el error
			alsSimanproError = new AlsSimanproError();
			alsSimanproError.setObjectName("ERROR");
			alsSimanproError.setDescription("REST Business Service returned HTTP response with status "
					+ responseSavePolicys.getStatusCode() + " No Body");
			alsSimanproError.setDateTime(new Date());
			alsSimanproError.setTransactionId(-1L);

			alsSimanproErrorService.create(alsSimanproError);
		}

	}

}
