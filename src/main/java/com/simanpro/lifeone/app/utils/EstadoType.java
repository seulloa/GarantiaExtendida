package com.simanpro.lifeone.app.utils;

/**
 * @author sehulloa on 08-03-2022
 *
 */
public enum EstadoType {
	
	PENDIENTE("PENDIENTE", 0),
	PROCESO("PROCESO", 1),
	PENDIENTE_CONSOLIDACION("PENDIENTE_CONSOLIDACION", 2),
	PROCESO_CONSOLIDACION("PROCESO_CONSOLIDACION", 3),
	CONSOLIDADO("CONSOLIDADO", 4),
	ERROR("ERROR", 5);
	
	String estado;
	int codigoEstado;

	EstadoType(String estado, int codigoEstado) {
		this.estado = estado;
		this.codigoEstado = codigoEstado;
	}
	
	public String getEstado() {
		return estado;
	}
	
	public int getCodigoEstado() {
		return codigoEstado;
	}
}
