package com.simanpro.lifeone.app.utils;

/**
 * @author sehulloa on 06-03-2022
 *
 */
public class FinalConstantParameter {

	public static final String ORACLE_QUERY_SCHEME = "T2ATEST";
	
	public static final String CVECTA_SV = "17";
	public static final String PLANN_SV = "24";
	
	public static final String CVECTA_GT = "20";
	public static final String PLANN_GT = "27";
	
	public static final String CVECTA_NI = "19";
	public static final String PLANN_NI = "26";
	
	public static final String CVECTA_CR = "18";
	public static final String PLANN_CR = "25";
	
	public static final Long URL_BASE_LIFEONE = 1L;
	public static final Long CONF_API_LIFEONE_SV = 1L;
	public static final Long CONF_API_LIFEONE_GT = 2L;
	public static final Long CONF_API_LIFEONE_NI = 3L;
	public static final Long CONF_API_LIFEONE_CR = 4L;
	
	public static final String URL_STATUS_LIFEONE = "/status";
	public static final String URL_SAVEPOLICYS_LIFEONE= "/savePolicys";
	
}
