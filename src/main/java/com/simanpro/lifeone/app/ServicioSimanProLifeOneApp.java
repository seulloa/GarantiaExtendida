package com.simanpro.lifeone.app;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.simanpro.lifeone.app.controller.AlsSimanproTrxController;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

import springfox.documentation.swagger2.configuration.Swagger2DocumentationConfiguration;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@SpringBootApplication
@Import(Swagger2DocumentationConfiguration.class)
public class ServicioSimanProLifeOneApp implements CommandLineRunner, WebMvcConfigurer {

	@Autowired
	AlsSimanproTrxController alsSimanproTrxController;
	
	private static Logger LOG = LoggerFactory.getLogger(ServicioSimanProLifeOneApp.class);

	public static void main(String[] args) {
		SpringApplication.run(ServicioSimanProLifeOneApp.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Locale.setDefault(new Locale("es", "SV"));
		
		LOG.info("STARTING THE APPLICATION");

		if (args.length > 0) {
			String vPais = args[0];
			LOG.info("EXECUTING : ServicioSimanProLifeOneApp for " + vPais);
			alsSimanproTrxController.ejecutarProceso(vPais);
		} else {
			LOG.error("ERROR: No se ha incluido el codigo de pais\n");
		}

		LOG.info("APPLICATION FINISHED");

	}
	
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
 
           registry.addResourceHandler("swagger-ui.html")
                    .addResourceLocations("classpath:/META-INF/resources/");
 
    }

}
