package com.simanpro.lifeone.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.simanpro.lifeone.app.entity.AlsSimanproTrx;
import com.simanpro.lifeone.app.utils.FinalConstantParameter;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Repository
public interface AlsSimanproTrxRepository extends JpaRepository<AlsSimanproTrx, Long> {
		
	
	@Query(
			value = "SELECT t.* FROM " + FinalConstantParameter.ORACLE_QUERY_SCHEME + ".ALS_SIMANPRO_TRX t WHERE t.cvecta = ?1 AND t.plann = ?2 And t.estado = ?3",
			nativeQuery = true)
	List<AlsSimanproTrx> findTrxByPais(String cvecta, String plann, String estado);

	
	@Modifying
	@Query(
			value = "UPDATE " + FinalConstantParameter.ORACLE_QUERY_SCHEME + ".ALS_SIMANPRO_TRX SET estado = ?4 WHERE cvecta = ?1 AND plann = ?2 And estado = ?3",
			nativeQuery = true)
	Integer UpdateEstadoTrx(String cvecta, String plann, String estado, String newEstado);
	
	
	@Modifying
	@Query(
			value = "UPDATE " + FinalConstantParameter.ORACLE_QUERY_SCHEME + ".ALS_SIMANPRO_TRX SET poliza = ?1 WHERE invoicenumber = ?2",
			nativeQuery = true)
	Integer UpdatePolizaByInvoiceNumber(String poliza, String invoiceNumber);
	
	
	@Modifying
	@Query(
			value = "UPDATE " + FinalConstantParameter.ORACLE_QUERY_SCHEME + ".ALS_SIMANPRO_TRX SET estado = ?1 WHERE invoicenumber = ?2",
			nativeQuery = true)
	Integer UpdateEstadoByInvoiceNumber(String estado, String invoiceNumber);

}
