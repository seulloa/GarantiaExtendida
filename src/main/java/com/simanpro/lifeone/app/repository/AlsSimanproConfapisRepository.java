package com.simanpro.lifeone.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.simanpro.lifeone.app.entity.AlsSimanproConfapis;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Repository
public interface AlsSimanproConfapisRepository extends JpaRepository<AlsSimanproConfapis, Long> {
		

}
