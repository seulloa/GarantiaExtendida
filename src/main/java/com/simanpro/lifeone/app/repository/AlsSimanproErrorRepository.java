package com.simanpro.lifeone.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.simanpro.lifeone.app.entity.AlsSimanproError;
import com.simanpro.lifeone.app.utils.FinalConstantParameter;

/**
 * @author sehulloa on 06-03-2022
 *
 */
@Repository
public interface AlsSimanproErrorRepository extends JpaRepository<AlsSimanproError, Long> {
	
	@Query(
			value = "SELECT " + FinalConstantParameter.ORACLE_QUERY_SCHEME + ".SEQ_SIMANPRO.nextval FROM dual", 
			nativeQuery = true)
	 Long getNextErrorId();

}
